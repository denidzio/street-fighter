export function runOnMultipleKeys(func, ...codes) {
  let handleKeyDown = (callback, codes, pressedKeys, e) => {
    if (!codes.includes(e.code)) {
      return;
    }

    pressedKeys.add(e.code);

    if (pressedKeys.size === codes.length) {
      callback();
    }
  };

  let handleKeyUp = (codes, pressedKeys, e) => {
    if (!codes.includes(e.code)) {
      return;
    }

    pressedKeys.delete(e.code);
  };

  const pressedKeys = new Set();

  handleKeyDown = handleKeyDown.bind(this, func, codes, pressedKeys);
  handleKeyUp = handleKeyUp.bind(this, codes, pressedKeys);

  document.addEventListener('keydown', handleKeyDown);
  document.addEventListener('keyup', handleKeyUp);

  return { handleKeyDown, handleKeyUp };
}

export function runOnSingleKey({ onPressFunc = () => {}, onReleaseFunc = () => {}, code }) {
  const handleKeyAction = (callback, e) => {
    if (e.code === code) {
      callback();
    }
  };

  const handleKeyDown = handleKeyAction.bind(this, onPressFunc);
  const handleKeyUp = handleKeyAction.bind(this, onReleaseFunc);

  document.addEventListener('keydown', handleKeyDown);
  document.addEventListener('keyup', handleKeyUp);

  return { handleKeyDown, handleKeyUp };
}

export function removeHandlers(handlers) {
  document.removeEventListener('keydown', handlers.handleKeyDown);
  document.removeEventListener('keyup', handlers.handleKeyUp);
}
