function changeHealthIndicator(indicator, fighter) {
  const indicatorWidth = fighter.health > 0 ? (fighter.health * 100) / fighter.fullHealth : 0;
  indicator.style.width = `${indicatorWidth}%`;
}

function attackAction(defender, hitPower) {
  defender.health -= hitPower;
  return defender.health <= 0;
}

export function critAttack(attacker, defender, defenderIndicator, getDamage) {
  if (attacker.inBlock) {
    return false;
  }

  const now = Date.now();

  if (now - attacker.lastCritHit < 10 * 1000) {
    return false;
  }

  const defenderIsKilled = attackAction(defender, getDamage(attacker));
  attacker.lastCritHit = now;

  changeHealthIndicator(defenderIndicator, defender);

  return defenderIsKilled;
}

export function simpleAttack(attacker, defender, defenderIndicator, getDamage) {
  if (attacker.inBlock || defender.inBlock) {
    return false;
  }

  const defenderIsKilled = attackAction(defender, getDamage(attacker, defender));
  changeHealthIndicator(defenderIndicator, defender);

  return defenderIsKilled;
}

export function blockEnable(fighter) {
  fighter.inBlock = true;
}

export function blockDisable(fighter) {
  fighter.inBlock = false;
}
