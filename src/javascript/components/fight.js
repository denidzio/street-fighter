import { controls } from '../../constants/controls';
import { critAttack, simpleAttack, blockEnable, blockDisable } from './fightAction';
import { runOnMultipleKeys, runOnSingleKey, removeHandlers } from '../helpers/keyboardHelper';

export async function fight(firstFighter, secondFighter) {
  const leftFighter = { ...firstFighter, fullHealth: firstFighter.health, inBlock: false, lastCritHit: 0 };
  const rightFighter = { ...secondFighter, fullHealth: secondFighter.health, inBlock: false, lastCritHit: 0 };

  const healthIndicatorNodes = {
    leftFighter: document.getElementById('left-fighter-indicator'),
    rightFighter: document.getElementById('right-fighter-indicator'),
  };

  return new Promise((resolve) => {
    const handlers = [];

    const handleIsWin = (attacker, isWin) => {
      if (isWin) {
        handlers.forEach((handler) => removeHandlers(handler));
        resolve(attacker);
      }
    };

    handlers.push(
      runOnMultipleKeys(() => {
        const isWin = critAttack(leftFighter, rightFighter, healthIndicatorNodes.rightFighter, getCritDamage);
        handleIsWin(leftFighter, isWin);
      }, ...controls.PlayerOneCriticalHitCombination)
    );

    handlers.push(
      runOnMultipleKeys(() => {
        const isWin = critAttack(rightFighter, leftFighter, healthIndicatorNodes.leftFighter, getCritDamage);
        handleIsWin(rightFighter, isWin);
      }, ...controls.PlayerTwoCriticalHitCombination)
    );

    handlers.push(
      runOnSingleKey({
        onPressFunc() {
          const isWin = simpleAttack(leftFighter, rightFighter, healthIndicatorNodes.rightFighter, getDamage);
          handleIsWin(leftFighter, isWin);
        },
        code: controls.PlayerOneAttack,
      })
    );

    handlers.push(
      runOnSingleKey({
        onPressFunc() {
          const isWin = simpleAttack(rightFighter, leftFighter, healthIndicatorNodes.leftFighter, getDamage);
          handleIsWin(rightFighter, isWin);
        },
        code: controls.PlayerTwoAttack,
      })
    );

    handlers.push(
      runOnSingleKey({
        onPressFunc() {
          blockEnable(leftFighter);
        },
        onReleaseFunc() {
          blockDisable(leftFighter);
        },
        code: controls.PlayerOneBlock,
      })
    );

    handlers.push(
      runOnSingleKey({
        onPressFunc() {
          blockEnable(rightFighter);
        },
        onReleaseFunc() {
          blockDisable(rightFighter);
        },
        code: controls.PlayerTwoBlock,
      })
    );
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}

export function getCritDamage(fighter) {
  return 2 * fighter.attack;
}
