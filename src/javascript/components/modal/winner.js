import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  showModal({
    title: `${fighter.name.toUpperCase()} won`,
    bodyElement: createFighterImage(fighter),
    onClose() {
      window.location.reload();
    },
  });
}
